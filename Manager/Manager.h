#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore/QTranslator>
#include <QtCore/QDir>
#include "Editor/Editor.h"
#include "Manager/ListModel/ListModel.h"


QT_BEGIN_NAMESPACE
namespace Ui { class Manager; }
QT_END_NAMESPACE

enum myView {
    todos = 0,
    visibles = 1,
    ocultos = 2
};

class Manager : public QMainWindow {
Q_OBJECT

public:
    Manager(QDir ubi, QWidget *parent = nullptr);
    void connectTraslator(QTranslator *tr);
    void retranslateUi();
    const QDir &getFilesDir() const;
    const QDir &getNotesDir() const;
    const QDir &getIconsDir() const;
    void updateIt();
    ~Manager();

    // Functions
private:
    void selectorIdiomas();
    QStringList listDirectory(QDir dir, QString extension = "*", bool listHidden = true);
    void doubleClickElement(const QModelIndex &index);
    void selectElement(const QModelIndex &index);
    void edit(QString e);
    bool validName(QString &e);
    void clearSelectionFromListView();
    void loadReloadImages();
    void about();
    void help();

    // Events
    virtual void closeEvent(QCloseEvent *event) override;

    // Lists
    void actionVisible();
    void actionHidden();
    void actionAll();

    // Button
    void actionAdd();
    void actionDel();
    void actionChangeName();
    void actionHide();

    // Directories
private:
    QDir _filesDir;
    QDir _notesDir;
    QDir _iconsDir;

    //Vars
private:
    Ui::Manager *ui;
    ListModel _model;
    QTranslator *_translator;
    Editor _editor;
    QStringList _list;
    int _selectIndex;
    myView _currentView;

};

#endif // MAINWINDOW_H
