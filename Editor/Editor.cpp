#include "Editor.h"
#include "ui_editor.h"
#include <QCloseEvent>
#include <QtWidgets/QtWidgets>
#include <Manager/Manager.h>
#include <QFile>
#include <QPrinter>
#include <QPrintDialog>


Editor::Editor(Manager *master, QWidget *parent) :
        QMainWindow(parent),
        ui(new Ui::Editor), _master(master), _running(false) {

    ui->setupUi(this);


    // Load Images
    this->loadReloadImages();


    //Connections

    connect(ui->actionCut, &QAction::triggered, ui->textEdit, &QPlainTextEdit::cut);
    connect(ui->actionCopy, &QAction::triggered, ui->textEdit, &QPlainTextEdit::copy);
    connect(ui->actionPaste, &QAction::triggered, ui->textEdit, &QPlainTextEdit::paste);
    connect(ui->actionUndo, &QAction::triggered, ui->textEdit, &QPlainTextEdit::undo);
    connect(ui->actionRedo, &QAction::triggered, ui->textEdit, &QPlainTextEdit::redo);

        // Print
    connect(ui->actionPrint,&QAction::triggered, this, &Editor::print);
        // Close and Save
    connect(ui->actionExit, &QAction::triggered, this, &QWidget::close);
    connect(ui->actionSave, &QAction::triggered, this, &Editor::saveFile);

    // Help
    connect(ui->actionHelp, &QAction::triggered, this, &Editor::help);
    connect(ui->actionAbout, &QAction::triggered, this, &Editor::about);
}

Editor::~Editor() {
    delete ui;
}


void Editor::loadReloadImages() {

    // Icon Windows
    this->setWindowIcon(QIcon(_master->getIconsDir().path() + "/" + tr("editor.png")));

    // Icons
    ui->actionSave->setIcon(QIcon(_master->getIconsDir().path() + "/" + tr("save.png")));
    ui->actionExit->setIcon(QIcon(_master->getIconsDir().path() + "/" + tr("quit.png")));
    ui->actionCut->setIcon(QIcon(_master->getIconsDir().path() + "/" + tr("cut.png")));
    ui->actionCopy->setIcon(QIcon(_master->getIconsDir().path() + "/" + tr("copy.png")));
    ui->actionPaste->setIcon(QIcon(_master->getIconsDir().path() + "/" + tr("paste.png")));
    ui->actionUndo->setIcon(QIcon(_master->getIconsDir().path() + "/" + tr("undo.png")));
    ui->actionRedo->setIcon(QIcon(_master->getIconsDir().path() + "/" + tr("redo.png")));
    ui->actionPrint->setIcon(QIcon(_master->getIconsDir().path() + "/" + tr("printer.png")));
    ui->actionHelp->setIcon(QIcon(_master->getIconsDir().path() + "/" + tr("help.png")));
    ui->actionAbout->setIcon(QIcon(_master->getIconsDir().path() + "/" + tr("about.png")));
}

void Editor::closeFile() {
    _master->setDisabled(false);
    this->clear();

}

bool Editor::open(QWidget *w, QString file) {
    _actualFile = file;

    QFile archivo(_master->getNotesDir().path() + QString("/").append(_actualFile));

    archivo.open(QIODevice::ReadOnly | QIODevice::Text);

    if (!archivo.isOpen()) {
        QMessageBox::critical(w, tr("Error"), archivo.errorString());
        return false;
    }

    QTextStream io;
    io.setDevice(&archivo);
    ui->textEdit->setPlainText(io.readAll());
    _saveChecker = ui->textEdit->toPlainText();
    archivo.flush();
    archivo.close();

    return true;
}

void Editor::saveFile() {

    QFile archivo(_master->getNotesDir().path() + QString("/").append(_actualFile));

    archivo.open(QIODevice::WriteOnly | QIODevice::Text);

    if (!archivo.isOpen()) {
        QMessageBox::critical(this, tr("Error"), archivo.errorString());
        return;
    }

    QTextStream io;
    io.setDevice(&archivo);

    _saveChecker = ui->textEdit->toPlainText();

    io << _saveChecker;
    archivo.flush();
    archivo.close();
}


void Editor::closeEvent(QCloseEvent *event) {

    if (_saveChecker != ui->textEdit->toPlainText()) {
        QMessageBox::StandardButton resBtn = QMessageBox::question(this, tr("Salir"),
                                                                   tr("¿Guardar nota?\n"),
                                                                   QMessageBox::Save | QMessageBox::Discard,
                                                                   QMessageBox::Save);


        if (resBtn == QMessageBox::Save) {
            this->saveFile();
        }

    }

    this->closeFile();
    _running = false;
    event->accept();
}


void Editor::showEvent(QShowEvent *event) {
    _running = true;
    event->ignore();

    if (_actualFile.isEmpty()) {
        throw std::invalid_argument("No file");
    }

}


bool Editor::isRunning() const {
    return _running;
}

void Editor::retranslateUi() {
    this->loadReloadImages();
    ui->retranslateUi(this);
}

void Editor::clear() {
    _saveChecker.clear();
    ui->textEdit->clear();
    _actualFile.clear();
}

void Editor::print() {

    QPrintDialog pd(this);
    if(pd.exec() != QDialog::Accepted) return;

    ui->textEdit->print(pd.printer());

}

void Editor::about() {

    QString about;

    QFile archivo(_master->getFilesDir().path() + QString("/resources/about/").append(tr("es.txt")));

    archivo.open(QIODevice::ReadOnly | QIODevice::Text);

    if (!archivo.isOpen()) {
        QMessageBox::critical(this, tr("Error"), archivo.errorString());
        return;
    }

    QTextStream io;
    io.setDevice(&archivo);

    about = io.readAll();

    archivo.flush();
    archivo.close();

    QMessageBox msgBox(this);
    msgBox.setWindowIcon(QIcon(_master->getIconsDir().path() + "/" + tr("about.png")));
    msgBox.setWindowTitle(tr("Acerca de"));
    msgBox.setTextFormat(Qt::RichText);
    msgBox.setText(about);
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.exec();

}

void Editor::help() {
    QDesktopServices::openUrl(QUrl(tr("https://www.arper.me/myqnote/help/es.html")));
}
