//
// Created by arper on 9/12/19.
//

#ifndef P9_IPO_LISTMODEL_H
#define P9_IPO_LISTMODEL_H

#include <QAbstractListModel>
#include <QtGui/QFont>

class ListModel : public QAbstractListModel {

Q_OBJECT

public:
    ListModel(QStringList *l, QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void updateView();

private:
    QStringList *_list;
    QFont _hiddenItemFont;
};


#endif //P9_IPO_LISTMODEL_H

