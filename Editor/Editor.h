#ifndef EDITOR_H
#define EDITOR_H

#include <QMainWindow>
#include <QtCore/QTextStream>

class Manager;

namespace Ui {
class Editor;
}

class Editor : public QMainWindow
{
    Q_OBJECT

public:
    explicit Editor(Manager* master, QWidget *parent = nullptr);
    void retranslateUi();
    bool open(QWidget* w, QString file);
    bool isRunning() const;
    ~Editor();

    // Functions
private:
    void closeFile();
    void saveFile();
    void clear();
    void print();
    void loadReloadImages();
    void about();
    void help();

    //Events
private:
    virtual void closeEvent(QCloseEvent *event) override;
    void showEvent(QShowEvent *event) override;

    //Vars
private:
    Ui::Editor *ui;
    Manager* _master;
    bool _running;
    QString _saveChecker;
    QString _actualFile;

};

#endif // EDITOR_H
