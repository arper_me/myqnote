## About myQNote

<div>Application icon designed by <a href="https://www.flaticon.es/autores/popcorns-arts" title="Icon Pond">Icon Pond</a>.</div>

<div>Application icons obtained in <a href="https://www.iconfinder.com/iconsets/tango-icon-library" title="Tango Icons">Tango Icons</a>.</div>

<div>Hide icon obtained in <a href="https://www.iconfinder.com/icons/315219/eye_hidden_icon" title="Hide Icon">Hidden Icon</a>.</div>
