//
// Created by arper on 9/12/19.
//

#include "ListModel.h"

ListModel::ListModel(QStringList *l, QObject *parent) : QAbstractListModel(parent), _list(l) {

    _hiddenItemFont.setItalic(true);

}

int ListModel::rowCount(const QModelIndex &parent) const {
    if (!_list) return 1;
    return _list->count();
}

QVariant ListModel::data(const QModelIndex &index, int role) const {

    switch (role) {
        case Qt::DisplayRole:
            if (!_list) return QString(tr("No hay lista cargada."));
            if (_list->at(index.row()).at(0) == '.') return QString(_list->at(index.row())).remove(".note").remove(0, 1);
            return QString(_list->at(index.row())).remove(".note");
        case Qt::FontRole:
            if (_list->at(index.row()).at(0) == '.') return _hiddenItemFont;
            break;
    }

    return QVariant();
}

void ListModel::updateView() {
    emit this->dataChanged(this->index(0), this->index(this->rowCount()));
    emit this->layoutChanged();
}

